<?php

ini_set('display_startup_errors', 1);
ini_set('display_errors', 1);
error_reporting(E_ALL);

ob_start(); 

try {
	$root_dir = dirname(dirname( __FILE__ ));

	include_once("$root_dir/monitor-libs/SystemMonitor.php");
	include_once("$root_dir/monitor-libs/FlightSessions.php");
	include_once("$root_dir/monitor-libs/GhettoCache.php");
	include_once("$root_dir/monitor-libs/VarLog.php");

	$misses = array();

	$f = new FlightSessions('api2', 'c6af7b9ef5c13869332319b7d9087fe5');

	$tunnels = $f->getTunnels();

	$tzmap = get_time_zone_map("$root_dir/system-monitors/tzids.csv");

	$i = isset($argv[1]) ? $argv[1] : 0;

	echo "Checking +$i days\n";
	$miss_count = 0;
	$misses = array();
	foreach ($tunnels as $t => $tunnel_data) {
		$tunnel = $tunnel_data['tunnel'];
		echo "\tChecking $tunnel ...";
		
		$has_failure = false;
		
		$time_zone = $tunnel_data['timezone'];
		foreach ($tzmap as $z => $timezone) {
			if ($timezone[2] == $time_zone) {
				echo "\n\t\tSetting timezone to $timezone[0]\n";
				date_default_timezone_set($timezone[0]);
				$time_zone = $timezone[0];
				break;
			}
		}
		
		for ($attempts = 0; $attempts < 2; $attempts++) {
			set_time_limit(200);
			try {
				$api2 = $f->GetApi2Sessions($tunnel, 'FLIGHTS   1ST_TIME  1STDOUBLE ', "+$i days");
				if (!is_array($api2) || count($api2) < 48) {
					echo "API 2 for $tunnel is not valid data: " . count($api2);
					$has_failure |= true;
				}
				$manifest_raw = $f->GetHelmetManifestSessions($tunnel, "+$i days");
				if (!is_array($manifest_raw) || count($manifest_raw) < 48) {
					echo "Manifest Raw for $tunnel is not valid data: " . count($manifest_raw);
					$has_failure |= true;
				}
				$manifest = $f->ApifyHelmet($manifest_raw);
				if (!is_array($manifest) || count($manifest) < 48) {
					echo "Manifest for $tunnel is not valid data: " . count($manifest);
					$has_failure |= true;
				}
			} catch (Exception $e) {
				print_r($e);
				$has_failure |= true;
			}
			if ($has_failure) {
				if ($attempts > 0)
					echo " tunnel failed, moving on in 30 seconds ...";
				else
					echo " tunnel failure, trying again in 30 seconds ...";
				sleep(30);
			} else {
				break;
			}
		}
		
		if ($has_failure) {
			echo " giving up on $tunnel\n";
			continue;
		}
		
		uasort($api2, function($a, $b) { return strtotime($a['session_time']) - strtotime($b['session_time']); });
		uasort($manifest, function($a, $b) { return strtotime($a['session_time']) - strtotime($b['session_time']); }); 
		uksort($manifest_raw, function($a, $b) { return strtotime($a) - strtotime($b); }); 

		$nowish = strtotime("+0 days");
		foreach ($api2 as $a => $entry) {
			$entry['qty_remaining'] = max(0, $entry['qty_remaining']);
			$session_time = strtotime(date("Y-m-d $entry[session_time]:00", $nowish));
			if ($session_time < time()) {
				$session_string = date("Y-m-d H:i:s", $session_time);
				continue;
			}
			if ($entry['session_time'] != $manifest[$a]['session_time']) {
				throw new \Exception("There was an error processing the results. $api2[session_time] does not equal " . $manifest[$a]['session_time']);
			}
			if (abs($entry['qty_remaining'] - $manifest[$a]['qty_remaining']) > 0) {
				if (abs($entry['qty_remaining'] - $manifest[$a]['qty_remaining']) > 1) {
					$date = date("Y-m-d", strtotime("+$i days"));
					if (!isset($misses[$date])) $misses[$i] = array();
					if (!isset($misses[$date][$tunnel])) $misses[$tunnel] = array();
					$raw_a = date('Y-m-d', strtotime("+$i days")) . " $entry[session_time]:00.000";
					$misses[$date][$tunnel][] = array(
							'Delta' => abs($entry['qty_remaining'] - $manifest[$a]['qty_remaining']),
							'Api2' => $entry,
							'Manifest' => $manifest[$a]
						);
					$miss_count++;
				}
				print_r(array(
						'Delta' => abs($entry['qty_remaining'] - $manifest[$a]['qty_remaining']),
						'Api2' => $entry,
						'Manifest' => $manifest[$a]
					));
			}
		}
		echo "\t\t" . $miss_count . " misses so far (total for all tunnels)\n";
	}

	if ($miss_count > 0) {
		$sent = false;
		for ($tries = 0; $tries < 3; $tries++) {
			echo "There were " .$miss_count . " misses\n";
			try {
				$monitor = new SystemMonitor('FlightSessions', 'Monitoring');
				$monitor->triggerPagerDuty(
						'7c5ddb5384fd4d53834df5b09b4b29e7', 
						'There was a time mis-match between API2::get_sessions() and the Manifest',
						'',
						$misses);
				$sent = true;
			} catch (Exception $e) {
				print_r($e);
			}
			if ($sent)
				break;
		}
	} else {
		echo "Full match\n";
	}
} catch (Exception $all) {
	print_r($all);
}
	
$output = ob_get_contents();
ob_end_clean();

$log = new VarLog('manifest-compare', 14);
$log->log($output);

function get_time_zone_map($map_file) {
	$handle = fopen($map_file,'r');
	$map = array();
	ini_set('auto_detect_line_endings',TRUE);
	if ($handle === false) {
		throw new \Exception("Could not open timezone file");
	}
	while ( ($data = fgetcsv($handle) ) !== FALSE ) {
		$map[] = $data;
	}
	ini_set('auto_detect_line_endings',FALSE);
	return $map;
}
